#!/bin/bash

set -e

# pull images first since we build in parallel
images=("exo1/dev-base-1" "exo1/dev-base-haskell" "exo1/dev-base-python" "exo1/dev-test-bitcoind")
for image in ${images[@]}; do
    docker pull "$image"  # NB: Don't pull in parallel since they depend on one another
done


bash ./_test_start.sh

bash ./_test_end.sh
