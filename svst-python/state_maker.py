#!/usr/bin/env python3

"""
This program is responsible for calculating state.

- loop forever
- count up n_votes, n_votes_for, n_votes_against
- cache in id=1 row of state_cache
- sleep
"""

import logging
import threading
import queue

# Initialise logging and print a header
import sys

logging.basicConfig(level=logging.INFO)
logging.warning("~~~~~~~~\n\n### State Maker Starting ###\n\n~~~~~~~~~")

import time

from database import get_state, get_n_valid_pallets, update_state, update_state_log, update_velocity_cache
from graceful_shutdown import should_shutdown
from environment import pallet_period, testing, is_producer


if is_producer:
    logging.error("Exiting as this appears to be a PRODUCER; killing self to avoid computational overhead")
    sys.exit(0)
else:
    logging.info("Starting up, PRODUCER set to %s", is_producer)


def should_update_state():
    last_state = get_state()
    n_valid_pallets = get_n_valid_pallets()
    return last_state.n_pallets != n_valid_pallets


def update_state_log_thread():
    run_every = 60  # 1 minutes
    if testing:
        run_every = 10
    while True:
        for seconds_since_tick in range(run_every):
            try:
                try:
                    if seconds_since_tick == 0:
                        update_state_log()
                        update_velocity_cache()
                except Exception as e:
                    logging.error("Update state failed: %s", e)
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    logging.info("%s %s %s", (exc_type, exc_tb.tb_frame.f_code.co_filename, exc_tb.tb_lineno))
                end_queue.get(block=True, timeout=1)  # this acts as our sleep
                logging.info("Ending update state log thread")
                return
            except queue.Empty:
                pass


end_queue = queue.Queue()
threads = [threading.Thread(target=update_state_log_thread)]
for thread in threads:
    thread.start()


TICK_PERIOD = min(3, pallet_period)  # seconds
while not should_shutdown():
    # Get a list of pallets from the pallets_downloaded table:
    if should_update_state():
        logging.info("Updating state %s", time.time())
        update_state()
        logging.info(" State updated %s", time.time())
    time.sleep(TICK_PERIOD)


for thread in threads:
    end_queue.put("time to die")
for thread in threads:
    thread.join()
