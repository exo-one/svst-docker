#!/usr/bin/env python3

"""
This program is responsible for validating and downloading pallet headers.

- loop forever
- access database to find all unprocessed nulldatas
- for each nulldata
  - extract header multihash
  - check header size (<200 bytes) against IPFS
  - check signature against central authority pubkey (env VERIFY_PUBKEY)
  - mark as processed in nd_processed always
  - insert into header_index if signature valid
    - if valid also insert pallet into pallet_index
  - in thread request from IPFS and on success add to header_processed
  - pin to IPFS too
"""

import json
import logging
import socket
import multiprocessing.pool
from functools import partial

logging.basicConfig(level=logging.INFO)
logging.warning("~~~~~~~~\n\n### Header Download Starting ###\n\n~~~~~~~~~")

import subprocess
import argparse
import time
import sys

import ipfsapi
import bitcoin.base58
import ed25519

import svst.utils as utils

from database import run_query, add_to_header_log, get_unprocessed_nd, \
    add_to_header_index, add_to_header_processed, add_to_pallet_index, reprocess_nd
from ipfs import ipfs, get_pin
from environment import magic_bytes, verify_pubkey_hex, pallet_period, is_producer
from graceful_shutdown import should_shutdown


if is_producer:
    logging.error("Exiting as this appears to be a PRODUCER; killing self to avoid computational overhead")
    sys.exit(0)
else:
    logging.info("Starting up, PRODUCER set to %s", is_producer)


MB_LENGTH = len(magic_bytes)


verify_pubkey = ed25519.VerifyingKey(verify_pubkey_hex, encoding='hex')
logging.info("Verification Pubkey: %s" % verify_pubkey_hex)


def process_nd_fully(nd_id, nd, block_time):
    mb = nd[:MB_LENGTH]
    ph_mh_bytes = nd[MB_LENGTH:]
    if len(ph_mh_bytes) != 34 or mb != magic_bytes.encode():
        logging.info("Nulldata not in required format (id=%s) %s", nd_id, nd)
        return
    logging.info("Nulldata acceptable, processing")
    header_mh = bitcoin.base58.encode(ph_mh_bytes)

    try:
        stat = ipfs.block_stat(header_mh)
        logging.info("Got IPFS stat: %s", stat)
        # technically headers will be 4 + 34 + 64 bytes = 102 (though IPFS reports 110...)
        if stat is not None:
            if stat['Size'] > 200:  # headers must be under 200 bytes
                logging.info("Nulldata failed size verification")
            else:
                add_to_header_index(header_mh, block_time)
        else:  # not found
            return reprocess_nd(nd_id)
    except Exception as e:
        logging.error("Encountered exception with ND %s: %s", nd_id, e)
        return
    logging.info("Processed ND: %s", nd_id)

    try:
        pallet_header = get_pin(header_mh)
        if pallet_header is not None:
            version = pallet_header[:4]
            pallet_mh_bytes = pallet_header[4:4+34]
            pallet_multihash = bitcoin.base58.encode(pallet_mh_bytes)
            sig = pallet_header[4+34:]
            assert len(version) == 4 and version == b'\x00'*4
            assert len(pallet_mh_bytes) == 34
            assert len(sig) == 64
            verify_pubkey.verify(sig, version + pallet_mh_bytes)  # throws an exception on bad signature
            valid = True
        else:  # not found
            return reprocess_nd(nd_id)
    except Exception as e:
        logging.info("Bad validation of header %s, %s" % (header_mh, e))
        valid = False
        pallet_multihash = ''

    add_to_header_processed(header_mh, valid, pallet_multihash)
    if valid:
        add_to_pallet_index(pallet_multihash)


thread_pool = multiprocessing.pool.ThreadPool(15)


TICK_PERIOD = min(5, pallet_period)  # seconds
while not should_shutdown():
    # process unprocessed nulldatas
    unprocessed_nd = get_unprocessed_nd()
    for nd_id, nd, block_time in unprocessed_nd:
        thread_pool.apply_async(process_nd_fully, args=(nd_id, nd, block_time))

    if len(unprocessed_nd) == 0:
        time.sleep(TICK_PERIOD)
