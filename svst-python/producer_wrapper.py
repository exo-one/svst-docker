#!/usr/bin/env python3

""" producer_wrapper.py

This script is a wrapper for our haskell pallet generator.

It will generate pallets and add them to IPFS, generate pallet headers and
add them to IPFS, construct the Bitcoin transactions necessary, and broadcast
said transactions.

Arguments this script take are:
 - secret key: hex bytes of the key to use to sign pallet hashes
 - pallet n: the number of pallets to generate
 - pallet hz: the frequency to generate pallets at
"""
import logging
logging.basicConfig(level=logging.INFO)
from functools import partial


import argparse
import time
import os
from decimal import Decimal
import multiprocessing.pool

import ipfsapi
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException, EncodeDecimal
import bitcoin.base58
import ed25519

import svst.utils as utils

from exceptions import raise_process_error, ProcessError
from process import sp_run
from bitcoind import bitcoind
from ipfs import ipfs


DEFAULT_N_BOXES = 10 * (1024**2) // 101 // 64  # 10 MB pallets
logging.info('Default %d boxes in each pallet' % DEFAULT_N_BOXES)
n_workers = 2
logging.info("Using %d workers" % n_workers)


def run_pallet_producer(n_boxes=DEFAULT_N_BOXES):
    completed_result = sp_run(['pallet-gen', str(n_boxes)])
    # logging.info(completed_result)
    if completed_result.returncode != 0 or b'PALLET_FILENAME' not in completed_result.stdout:
        raise_process_error(completed_result)
    filename = list(filter(lambda l: b'PALLET_FILENAME' in l, completed_result.stdout.splitlines()))[0].split(b':')[-1]
    return filename.decode()


def run_pallet_header_producer(pallet_multihash, secret_key_hex):
    random_filename = utils.gen_header_filename()
    completed_result = sp_run(['pallet-header', '-o', random_filename, pallet_multihash, '-k', secret_key_hex])
    if completed_result.returncode != 0:
        raise_process_error(completed_result)
    return random_filename


def add_and_pin_to_ipfs_then_rm(filename):
    res = ipfs.add(filename)
    ipfs.pin_add(res['Hash'])
    os.remove(filename)
    return res


def keep_trying(f, exception_options, name="", delay=3.0, n=999999):
    while n > 0:
        try:
            return f()
        except exception_options as e:
            logging.warning("Keep trying %s failed, %d times to go" % (name, n))
            time.sleep(delay)
            n -= 1


def fund_raw_tx(raw_tx, fee_rate):
    return bitcoind.fundrawtransaction(raw_tx, {'lockUnspents': True, 'feeRate': fee_rate})


def bitcoin_publish_nulldata(nd, fee_rate):
    raw_tx = bitcoind.createrawtransaction([], {"data": utils.hexlify(nd).decode()})
    # logging.info("Created raw tx: %s" % raw_tx)
    funded_tx = keep_trying(partial(fund_raw_tx, raw_tx, fee_rate), JSONRPCException, name="fund_raw_transaction", n=99999)
    # logging.info("Funded raw tx: %s" % funded_tx)
    signed_tx = bitcoind.signrawtransaction(funded_tx['hex'])
    # logging.info(json.dumps(signed_tx, indent=2))
    # logging.info(json.dumps(bitcoind.decoderawtransaction(signed_tx['hex']), indent=2, default=EncodeDecimal))
    # logging.info("BTC per KB: %f" % (float(funded_tx['fee']) * 1024.0 / len(signed_tx['hex']) / 2))
    txid = bitcoind.sendrawtransaction(signed_tx['hex'])
    logging.info("Sent transaction: %s" % txid)
    return txid


def produce_one_pallet(pallet_period, fee_rate, box_per_pallet, curr_pallet_n, secret_key):
    start = time.time()
    logging.info("Constructing pallet %d" % (curr_pallet_n + 1))

    pallet_filename = run_pallet_producer(box_per_pallet)
    logging.info("Generated pallet w filename %s" % pallet_filename)
    ipfs_pallet_res = add_and_pin_to_ipfs_then_rm(pallet_filename)
    logging.info("Added pallet to IPFS: %s" % ipfs_pallet_res['Hash'])
    pallet_header_filename = run_pallet_header_producer(ipfs_pallet_res['Hash'], secret_key)
    logging.info("Generated pallet header w filename %s" % pallet_header_filename)
    ipfs_pallet_header_res = add_and_pin_to_ipfs_then_rm(pallet_header_filename)
    ipfs_pallet_header_bytes = bitcoin.base58.decode(ipfs_pallet_header_res['Hash'])
    logging.info("Added pallet header to IPFS: %s %s" % (ipfs_pallet_header_res['Hash'], ipfs_pallet_header_bytes))
    nulldata = MAGIC_BYTES + ipfs_pallet_header_bytes
    bitcoin_publish_nulldata(nulldata, fee_rate)

    end = time.time()
    logging.info("Pallet %s production took %s seconds", curr_pallet_n, end - start)

    # sleep whatever time is left between now and the next pallet as calculated by the period
    time.sleep(max(0, (start + pallet_period) - time.time()))


if __name__ == "__main__":
    logging.warning("~~~~~~~\n\n### PRODUCER STARTING NOW ###\n\n~~~~~~~")

    parser = argparse.ArgumentParser()
    parser.add_argument('--secret-key', type=str, required=True,
                        help='The secret key to sign pallet headers with; in hex')
    parser.add_argument('--btc-rpc-user', type=str, required=True, help='RPC user for bitcoind')
    parser.add_argument('--btc-rpc-password', type=str, required=True, help='RPC password for bitcoind')
    parser.add_argument('--fee-rate', type=float, default=0.001, help='BTC per KB to use for BTC tx')
    parser.add_argument('--magic-bytes', type=str, required=True, help='Magic bytes for voting network')
    parser.add_argument('--pallet-n', type=int, required=True)
    parser.add_argument('--pallet-period', type=float, default=1.0)
    parser.add_argument('--box-per-pallet', type=int, default=DEFAULT_N_BOXES)
    parser.add_argument('--not-before', type=int, default=0)

    args = parser.parse_args()
    assert args.pallet_n > 0
    logging.info("Making %d pallets with %d boxes each" % (args.pallet_n, args.box_per_pallet))
    assert args.fee_rate < 0.003  # sanity check, < 0.003 BTC / KB
    logging.info("Using magic bytes: %s" % args.magic_bytes)

    MAGIC_BYTES = args.magic_bytes.encode()

    worker_pool = multiprocessing.pool.Pool(n_workers)

    funding_addr = bitcoind.getnewaddress()
    funding_addr_privkey = bitcoind.dumpprivkey(funding_addr)
    logging.info("Got finding addr: %s", funding_addr)
    logging.info("Private key for addr: %s", funding_addr_privkey)

    logging.info("Verify secret key starts with %s; ends with %s", args.secret_key[:2], args.secret_key[-2:])
    pubkey = ed25519.SigningKey(args.secret_key.encode(), encoding='hex').get_verifying_key().to_ascii(encoding='hex')
    logging.info("Verify public key is %s", pubkey)

    not_before = args.not_before
    logging.info("Not starting before: %s which is %s seconds away", not_before, not_before - time.time())
    while time.time() < not_before:
        logging.info("Send BTC to: %s", funding_addr)
        logging.info("Current balance: %s", bitcoind.getbalance())
        now = int(time.time())
        logging.info("Waiting till %s, current time %s, difference %s seconds or %s minutes",
                     not_before, now, not_before - now, (not_before - now) / 60)
        logging.info("Parameters: %s pallets, %s boxs_per_pallet, %s pallet_period",
                     args.pallet_n, args.box_per_pallet, args.pallet_period)
        time.sleep(10)
    start_time_global = time.time()

    for curr_pallet_n in range(args.pallet_n):
        worker_pool.apply_async(produce_one_pallet, (args.pallet_period * n_workers, args.fee_rate,
                                                     args.box_per_pallet, curr_pallet_n, args.secret_key))

    worker_pool.close()
    worker_pool.join()
    end_time = time.time()
    duration = (end_time - start_time_global)
    logging.info("Produced %s pallets over %s minutes at a period of 1 pallet every %s seconds",
                 args.pallet_n, duration/60, duration/args.pallet_n)

    refund_addr = os.environ.get("BTC_REFUND", None)
    if refund_addr is not None:
        logging.info("Sending refund to %s", refund_addr)
        balance = bitcoind.getbalance("*", 0, False) - Decimal(0.001)
        # this fails in regtest mode due to 1000s of inputs
        logging.info("Sending balance %s to %s", balance, refund_addr)
        txid = bitcoind.sendtoaddress(refund_addr, balance)
        logging.info("Sent refund in TX %s", txid)
    logging.info("\n\nPallet gen wrapper complete.\n\n")
