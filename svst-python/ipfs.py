import logging
import time
import multiprocessing
import multiprocessing.pool
import traceback

import ipfsapi


def _ipfs_full_call(host, name, *args, **kwargs):
    # logging.info("IPFS Call: %s, %s, %s" % (host, name, args))
    return ipfsapi.Client(host=host).__getattribute__(name)(*args, **kwargs)


class RPCCall:
    def __init__(self, host, name):
        self.host = host
        self.name = name

    def __call__(self, *args, **kwargs):
        while True:
            try:
                return _ipfs_full_call(self.host, self.name, *args, **kwargs)
            except ipfsapi.exceptions.TimeoutError as e:
                logging.error('Timeout - IPFS')
                return None
            except Exception as e:
                logging.error("(sleeping 5) IPFSWrapper encounted an error: %s", repr(e))
                time.sleep(5)


class IPFSWrapper:
    def __init__(self, host, *args, **kwargs):
        self.host = host

    def __getattr__(self, item):
        return RPCCall(self.host, item)


def make_ipfs(host="ipfs"):
    return IPFSWrapper(host)


ipfs = make_ipfs()


def pin(multihash):
    ipfs.pin_add(multihash)
    # logging.info("Pinned %s" % multihash)


def get_pin(multihash):
    # logging.info("Retrieving file from IPFS: %s" % multihash)
    file_bytes = ipfs.cat(multihash)
    # logging.info("Got file of length %d" % len(file_bytes))
    pin(multihash)
    return file_bytes


def _test_cat(mh):
    bs = ipfs.cat(mh, with_timeout=False)
    return mh


def _test_block_stat(mh):
    ipfs.block_stat(mh, with_timeout=False)
    return mh


def ipfs_get_pin_many_concurrently(mhs, no_bytes=False):
    good_mhs = _ipfs_something_concurrently(mhs, _test_cat, no_bytes=no_bytes)
    return list([(mh, pin(mh) if no_bytes else get_pin(mh)) for mh in good_mhs])


def ipfs_block_stat_concurrently(mhs):
    statd_mhs = _ipfs_something_concurrently(mhs, _test_block_stat)
    return list([(mh, ipfs.block_stat(mh)) for mh in statd_mhs])


def _ipfs_something_concurrently(mhs, para_func, no_bytes=False):
    assert type(mhs) is list
    # logging.info("Starting IPFS  concurrently w %s mhs", len(mhs))

    if len(mhs) == 0:
        return []

    pool = multiprocessing.pool.Pool(50)

    results = list((mh, pool.apply_async(para_func, (mh,))) for mh in mhs)
    # logging.info("Get_pin_concurrent mhs: %s", mhs)
    pool.close()

    logging.info("IPFS waiting for results (max 20s)")
    for second in range(20):
        try:
            for mh, result in results:
                if not result.ready():
                    raise TimeoutError()
            break
        except TimeoutError as e:
            time.sleep(1)

    good_mhs = []
    for mh, result in results:
        try:
            if result.ready():
                # logging.info("Got good result: %s", mh)
                res = result.get(0)
                if res is not None:
                    good_mhs.append(res)
            else:
                logging.info("Failed to get %s", mh)
        except Exception as e:
            logging.error("Encountered error in ipfs concurrent get: %s", e)
            continue

    logging.info("ipfs_concurrent got %s multihashes out of %s", len(good_mhs), len(mhs))
    return good_mhs

