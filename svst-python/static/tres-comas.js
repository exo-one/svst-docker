var SCREEN_WIDTH = window.innerWidth,
    SCREEN_HEIGHT = window.innerHeight;

function tresComas() {

    // Three Coma Club Image
    var club = document.createElement("img");
    club.src = "three-comma-club.jpg";
    club.style.position = "absolute";
    club.style.left = SCREEN_WIDTH/2 - 500/2 + "px";
    club.style.top = SCREEN_HEIGHT/2 - 440/2 +  150 + "px";
    club.style.opacity = 0;
    document.body.appendChild(club);  

    var comas = []
    for (var i = 0; i < 3; i++) {
        var img = document.createElement("img");
        img.src = "coma.png";
        img.style.position = "absolute";
        img.style.left = SCREEN_WIDTH/2 - 215/2 + "px";
        img.style.top = SCREEN_HEIGHT/2 - 275/2 - 150 + "px";
        document.body.appendChild(img); 
        comas.push(img);       
    }
    var billion = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0];   

    TweenLite.from(comas, 2, {rotation:1080, scaleX:0, scaleY:0, ease:Back.easeOut.config(1)});

    TweenLite.to(comas[0], 1, {x:"-=300px", delay:1.5});
    TweenLite.to(comas[1], 1, {x:"+=300px", delay:1.5});
    TweenLite.to(club, 2, {opacity:1, delay:3});
    TweenLite.to(comas, 1, {scaleX:0, scaleY:0, delay:18});
    TweenLite.to(club, 1, {opacity:0, delay:18});
}

