//
// MAIN
//
var flux = this;
flux.plotData = [{
    x: [], 
    y: [], 
    type: 'bar', 
    name: "State votes per Minute", 
    dataName: "state_vps",
    marker: {color:"#439CD6"}
}];
flux.graphOptions = {
    title: 'Votes per Minute',
    margin:{l:40, r:10, b:20, t:40},
    plot_bgcolor: "rgba(255,255,255,0.1)",
    paper_bgcolor: "rgba(255,255,255,0.1)",
    font: {
        family: "'LatoLatinWebLight', verdana, arial, sans-serif",
        color: "#fff"
    },
    xaxis: {
        color: "#E3580D",
        tickfont: {color: "white"},
        tickangle:0,
        nticks:8
    },
    yaxis: {
        color: "#E3580D",
        tickfont: {color: "white"},
        showline: true
    },
    width:396,
    height:296
};

flux.updateGraphs = function(data){
    flux.plotData[0].x = makeTimeArray(data, "last_processed").reverse();
    flux.plotData[0].y = makeDataArray(data, flux.plotData[0].dataName, 60).reverse();
    if (document.getElementById('voteVelocityGraph')) {
        Plotly.redraw('voteVelocityGraph');
    }
}

flux.loadVPHGraph = function(json){
    flux.graphOptions.title = 'Votes per Hour';
    flux.plotData[0].x = makeDataArray(json, "time", 1)
    flux.plotData[0].y = makeDataArray(json, "vph", 1)
    if (document.getElementById('voteVelocityGraph')) {
        Plotly.redraw('voteVelocityGraph');
    }
}

flux.generateGraphs = function(){
    Plotly.newPlot('voteVelocityGraph', flux.plotData, graphOptions);
}

flux.generateGraphs();
// tresComas();


// Modals
var modal = document.getElementById('myModal');
var listOfModals = ["flux", "xo1", "secure.vote", "faq", "txs", "vpm", "pallets"]
for (var i in listOfModals) {
    var btn = document.getElementById(listOfModals[i]);
    if (typeof btn !== "undefined" && btn !== null) {
        btn.onclick = function(event) {
            var listOfModals = ["flux", "xo1", "secure.vote", "faq", "txs", "vpm", "pallets"]
            for (var j in listOfModals) {
                var elem = document.getElementById(listOfModals[j] + "-content")
                if (typeof elem !== "undefined" && elem !== null) {
                    elem.style.display = "none";
                }
            }
            modal.style.display = "block";
            document.getElementById(event.target.id + "-content").style.display = "block";
        }
    }
}
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}