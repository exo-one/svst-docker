//
// UTILS
//


// http://stackoverflow.com/questions/5448545/how-to-retrieve-get-parameters-from-javascript
function getParam(val) {
    var result = undefined,
        tmp = [];
    location.search
        //.replace ( "?", "" )
        // this is better, there might be a question mark inside
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
        });
    return result;
}


//
// DEBUG
//

var __debug__ = getParam('debug') !== undefined;  // just include ?debug=1 in the URL
var debug = {};
debug.scaling = 1.0;
var alertId = 0;

if (__debug__) {
    debug.getinfo = [];
    for(var _i = 0; _i < 144 * debug.scaling; _i++){
        var n_pallets = 70 * debug.scaling
        var votes_p_pallet = 150000 * debug.scaling
        debug.getinfo.push({
            'n_pallets': n_pallets * _i,
            'n_votes': n_pallets * _i * votes_p_pallet,
            'votes_for': n_pallets * _i * votes_p_pallet / 2,
            'votes_against': n_pallets * _i * votes_p_pallet / 2,
        })
    }
    debug.getvelocity = {"velocity_data": []};
    var length = 144;
    for(var _i = 0; _i < length; _i++){
        debug.getvelocity.velocity_data.push({
            'pallet_vps': 0,
            'state_vps': Math.random()*10000+20000,
            'id': length-_i,
            'header_vps': 0,
            'last_processed': new Date().getTime() - _i*60
        });
    }
}

function generateDebugAlert() {
    console.log("Generating Alert");
    alertId++;
    return {
        "alert": true,
        "msg": "Test Alert" + alertId,
        "id": alertId
    }
}

//
// ANGULAR
//

var app = angular.module('svstApp', ['ngOdometer']);

app.controller('SvstController', function($scope, $rootScope, $log, $http, $window){
    $rootScope._ = _;
    $scope._ = _;
    var ctrl = this;
    ctrl.tab = 'intro';
    ctrl.log = [];  // log for msgs, {'type': 'error', 'msg': 'Test Error'}
    ctrl.tmp = {};
    ctrl.tres_comas = false;
    ctrl.testStarted = false;
    ctrl.startTime = 1488812400 * 1000;
    ctrl.vpm = 0;
    ctrl.peakvpm = 3960300;
    ctrl.totalDuration = niceTime(98923000);
    ctrl.testFinished = true;
    ctrl.showAlert;

    ctrl.setTab = function(newTab){ ctrl.tab = newTab; }
    ctrl.isTab = function(someTab){ return someTab == ctrl.tab; }

    ctrl.getinfo = {
        'n_pallets': 0,
        'n_votes': 0,
        'votes_for': 0,
        'votes_against': 0,
    }
    ctrl.getvelocity = [{
        'header_vps': 0, 
        'id': 0, 
        'pallet_vps': 0, 
        'last_processed': 0, 
        'state_vps': 0
    }]
    ctrl.alert = {
        'alert': false,
        'msg': "",
        'id': 0
    }
    ctrl.nationCount = {
        'aus':"0.0",
        'usa':"0.0",
        'eur':"0.0",
        'ind':"0.0"
    }
    //TODO: Use real numbers
    ctrl.nationPopulation = {
        'aus':15787514, // Total Population: 24380458 %ofPopualtion: 0.6475
        'usa':231556622, // Total Population: 325706850 %ofPopualtion: 0.7109 
        'eur':481000000, // Total Population: 740000000 %ofPopualtion: 0.6500
        'ind':814500000 // Total Population: 1326801576 %ofPopualtion: 0.6138
    }
    ctrl.txs = ["9a461696da724dda27c06595d2d22a33befbf955ccde550a8195861f0eb466af"];
    ctrl.previous_getinfo = ctrl.getinfo
    ctrl.previous_alert = ctrl.alert

    ctrl.errorHandler = function(err){
        ctrl.log.unshift({type: "error", msg: err, hide: false});
        $log.log("Error", err);
        $log.log(err);
    }
    ctrl.addMsg = function(msg){
        ctrl.log.unshift({type: "msg", msg: msg, hide: false})
        $log.log("added msg", msg);
    }

    ctrl.gotNewGetInfo = function(data){
        ctrl.previous_getinfo = ctrl.getinfo;
        ctrl.getinfo = data.data;
        console.log("Loaded data: ", data);
        ctrl.updateCountries();

        // Check for Tres Comas
        if (!ctrl.tres_comas && data.data.n_votes > 1000000000) {
            console.log("tres comas");
            ctrl.tres_comas = true
            var launchInterval = setInterval(launch, 800);
            var loopInterval = setInterval(loop, 1000 / 50);
            // tresComas();
            // setTimeout(function() {
            celebrating = false;
            launchSpeed = 0.2;
            // }, 1*60*1000); // 1 Minute timeout for fireworks
        }
    }

    ctrl.gotNewGetVelocity = function(data){
        ctrl.previous_getvelocity = ctrl.getvelocity;
        ctrl.getvelocity = data.data.velocity_data;
        ctrl.vpm = Math.round(ctrl.getvelocity[0].state_vps)*60;
        console.log("Loaded Velocity data: ", data);
        flux.updateGraphs(ctrl.getvelocity.slice(0,60));
    }

    ctrl.gotNewAlert = function(data){
        ctrl.previous_alert = ctrl.alert;
        ctrl.alert = data.data;
        if (ctrl.previous_alert.id != ctrl.alert.id) {
            ctrl.showAlert = true;
        }
        console.log("Loaded data: ", data);
    }

    ctrl.gotRecentTxs = function(data){
        ctrl.prev_txs = ctrl.txs;
        ctrl.txs = data.data.txs;
        console.log("Loaded data: ", data);
    }

    ctrl.startingIn = function(){
        var countdown = ctrl.startTime - new Date().getTime();
        if (countdown <= 0) {
            ctrl.testStarted = true;
        }
        return niceTime(countdown);
    }

    ctrl.timeRunning = function(){
        return niceTime(new Date().getTime() - ctrl.startTime);
    }

    ctrl.updateCountries = function(data){
        for (var key in ctrl.nationPopulation) {
            if (ctrl.nationPopulation.hasOwnProperty(key)) {
                ctrl.nationCount[key] = (ctrl.getinfo.n_votes / ctrl.nationPopulation[key]).toFixed(1);
            }
        }
        console.log(ctrl.nationCount);
    }

    ctrl.loadData = function(){
        if (!ctrl.testFinished) {
            if (__debug__){
                ctrl.addMsg("New debug getinfo " + (Date.now() / 1000).toString())
                ctrl.gotNewGetInfo({data: debug.getinfo.shift()})
                ctrl.gotNewGetVelocity({data: debug.getvelocity})
                if(__debug__ && Math.random()<0.5){ctrl.gotNewAlert({data: generateDebugAlert()})}
            } else {
                $http.get('/getinfo').then(ctrl.gotNewGetInfo, ctrl.errorHandler);
                $http.get('/getvelocity').then(ctrl.gotNewGetVelocity, ctrl.errorHandler);
                $http.get('/recenttxs').then(ctrl.gotRecentTxs, ctrl.errorHandler);
                $http.get('https://xo1.io/stress-test-alert.json').then(ctrl.gotNewAlert, ctrl.errorHandler);
            }
            setTimeout(ctrl.loadData, 3000);
        } else {
            $.getJSON('getinfo.json').then(ctrl.gotNewGetInfo, ctrl.errorHandler);
            $.getJSON('velocityresults.json').then(flux.loadVPHGraph, ctrl.errorHandler);
            $.getJSON('recenttxs.json').then(ctrl.gotRecentTxs, ctrl.errorHandler);
        }
        $scope.$apply();
    }
    setTimeout(ctrl.loadData, 1000);

    ctrl.updateCountdownAndScope = function(){
        $scope.$apply();
        setTimeout(ctrl.updateCountdownAndScope, 200);
    }
    setTimeout(ctrl.updateCountdownAndScope, 200);
});
