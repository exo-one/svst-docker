#!/bin/bash

if [[ $VERIFY_SECRET_KEY ]]; then
    echo "Producer starting... with not_before: $NOT_BEFORE"
    ./producer_wrapper.py --secret-key $VERIFY_SECRET_KEY --btc-rpc-user $RPCUSER \
        --btc-rpc-password $RPCPASSWORD --fee-rate 0.00225 --magic-bytes $MAGICBYTES \
        --box-per-pallet $BOX_P_PALLET --pallet-n $NUM_PALLETS --pallet-period $PALLET_PERIOD \
        --not-before $NOT_BEFORE
else
    echo "Producer doing nothing..."
fi
