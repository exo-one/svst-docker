#!/usr/bin/env python3

"""
This program is responsible for downloading pallets.

- loop forever
- access database to find all undownloaded pallets
- for each pallet from pallet_index
  - in a new thread:
  - request pallet from IPFS
  - pin pallet to IPFS
  - once downloaded mark in pallet_downloaded
"""

import logging
logging.basicConfig(level=logging.INFO)
logging.warning("~~~~~~~~\n\n### Pallet Download Starting ###\n\n~~~~~~~~~")

import time
import sys
import multiprocessing.pool


from database import run_query, get_pallets_not_downloaded, mark_pallet_downloaded, redownload_pallet
from ipfs import get_pin
from environment import pallet_period, is_producer
from graceful_shutdown import should_shutdown


if is_producer:
    logging.error("Exiting as this appears to be a PRODUCER; killing self to avoid computational overhead")
    sys.exit(0)
else:
    logging.info("Starting up, PRODUCER set to %s", is_producer)


def download_pallet(pallet_mh):
    try:
        pallet = get_pin(pallet_mh)
        if pallet is not None:
            valid = True
        else:
            return redownload_pallet(pallet_mh)
    except Exception as error:
        logging.info("Unable to download pallet %s, %s", pallet_mh, error)
        valid = False
    if valid:
        mark_pallet_downloaded(pallet_mh)


thread_pool = multiprocessing.pool.ThreadPool(15)


TICK_PERIOD = min(5, pallet_period)  # seconds
while not should_shutdown():
    pallets_to_test = get_pallets_not_downloaded()

    for pallet_mh, in pallets_to_test:
        thread_pool.apply_async(download_pallet, (pallet_mh,))

    time.sleep(TICK_PERIOD)
