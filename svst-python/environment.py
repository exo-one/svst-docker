import os

from fancy_log import fancy_log


magic_bytes = os.getenv('MAGICBYTES')
dbname = os.getenv('POSTGRES_DB')
dbhost = os.getenv('POSTGRES_HOST')
dbuser = os.getenv('POSTGRES_USER')
NUM_PALLETS = int(os.getenv('NUM_PALLETS', 20))
BOX_P_PALLET = int(os.getenv('BOX_P_PALLET', 1000))
rpcuser = os.getenv('RPCUSER')
rpcpassword = os.getenv('RPCPASSWORD')
verify_pubkey_hex = os.getenv('VERIFY_PUBKEY')
verify_secret_key_hex = os.getenv('VERIFY_SECRET_KEY', None)
pallet_period = float(os.getenv('PALLET_PERIOD'))
start_block = int(os.getenv('START_BLOCK', 0))
not_before = int(os.getenv('NOT_BEFORE', 0))

is_producer = os.getenv('PRODUCER', '').lower() == 'true'
testing = os.getenv('TESTING', 'false').lower() == 'true'
if testing:
    fancy_log("Environment.py TESTING", 'TESTING ENABLED')

MB_LEN = len(magic_bytes)
