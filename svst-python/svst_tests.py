from log import logging

import datetime
import requests
import time
import json
import socket
import sys, os
from pprint import pformat

import psycopg2

import bitcoin
import bitcoin.base58
from bitcoinrpc.authproxy import AuthServiceProxy, EncodeDecimal, JSONRPCException

from fancy_log import fancy_log
from database import *
from bitcoind import bitcoind
from ipfs import make_ipfs
from environment import magic_bytes, NUM_PALLETS, pallet_period, verify_secret_key_hex
from producer_wrapper import bitcoin_publish_nulldata, run_pallet_header_producer, run_pallet_producer, \
    add_and_pin_to_ipfs_then_rm
# Variables to store test state
last_scrape_n = 0
last_header_n = 0
last_pallet_index_n = 0
last_pallet_downloaded_n = 0
last_pallet_valid_n = 0
last_pallet_invalid_n = 0
last_state = tuple()


def step_00_bitcoind():
    """Generate coins and confirm transactions"""
    bitcoind.generate(31)
    getinfo = bitcoind.getinfo()
    fancy_log("Limited Bitcoin Getinfo", {'blocks': getinfo['blocks'], 'balance': getinfo['balance']})
    return getinfo


def step_10_scraper():
    """Update scraper stats and ensure data 'looks' good"""
    global last_scrape_n
    all_scrapes = run_query("SELECT id, prefix, tx_id, data, block_time, block_hash FROM null_data ORDER BY id ASC")
    fancy_log("Last Scrape", [] if len(all_scrapes) == 0 else all_scrapes[-1][:2])
    logging.info("All scrapes: %s", len(all_scrapes))
    assert len(all_scrapes) >= last_scrape_n
    last_scrape_n = len(all_scrapes)
    # for id, prefix, txid, nd, btime, bhash in all_scrapes:  # TODO: Where to put these?
    #     assert nd[:len(magic_bytes)] == magic_bytes.encode()
    #     assert len(nd) == 40


def step_20_pallet_headers():
    """Check pallet headers are downloading"""
    global last_header_n
    all_headers = run_query("SELECT * FROM header_processed")
    logging.info("Got %d headers in header_processed" % len(all_headers))
    fancy_log("Last Header", [] if len(all_headers) == 0 else all_headers[-1])
    for id, header_multihash, valid, pallet_multihash in all_headers:
        assert len(bitcoin.base58.decode(header_multihash)) == 34
        if valid:
            assert len(bitcoin.base58.decode(pallet_multihash)) == 34
        else:
            assert len(pallet_multihash) == 0
    last_header_n = len(run_query("SELECT * FROM header_processed WHERE valid = true"))
    logging.info("Got %d valid headers" % last_header_n)


def step_30_pallet_index():
    """Check pallet index is recieving pallets"""
    global last_pallet_index_n
    all_pallets_in_index = run_query("SELECT * FROM pallet_index")
    last_pallet_index_n = len(all_pallets_in_index)
    logging.info("Got %d pallets in index" % last_pallet_index_n)


def step_35_pallet_download():
    # Check pallet downloads:
    global last_pallet_downloaded_n
    all_pallets_downloaded = run_query("SELECT * FROM pallet_downloaded")
    logging.info("Pallet_downloaded rows: %s", len(all_pallets_downloaded))
    fancy_log("Last Pallet", [] if len(all_pallets_downloaded) == 0 else all_pallets_downloaded[-1])
    # Ensure all downloaded pallets exist in the index
    pallets_absent_from_index = run_query(" SELECT pallet_multihash FROM pallet_downloaded"
                                          " WHERE pallet_multihash NOT IN"
                                          " (SELECT pallet_multihash from pallet_index)")
    assert len(pallets_absent_from_index) == 0
    last_pallet_downloaded_n = len(all_pallets_downloaded)


def step_40_pallet_validation():
    global last_pallet_valid_n, last_pallet_invalid_n
    # test validated pallets
    all_valid_pallets = get_valid_pallets()
    last_pallet_valid_n = len(all_valid_pallets)
    for vp in all_valid_pallets:
        assert len(run_query("SELECT * FROM pallet_index WHERE pallet_multihash = %s", (vp.pallet_multihash,))) == 1
        assert vp.n_votes == vp.votes_for + vp.votes_against
    all_invalid_pallets = get_invalid_pallets()
    last_pallet_invalid_n = len(all_invalid_pallets)
    assert len(run_query("SELECT * FROM pallet_validated_log")) == (last_pallet_valid_n + last_pallet_invalid_n)
    fancy_log("Last Valid Pallet", [] if len(all_valid_pallets) == 0 else all_valid_pallets[-1])


def step_50_check_state():
    """Just get and print state - assertions at end"""
    global last_state
    state = get_state()
    fancy_log("Current State", state)
    last_state = state


def step_60_check_ipfs():
    """Use the second IPFS node to test downloads from the first IPFS node"""
    ipfs1 = make_ipfs()
    ipfs2 = make_ipfs("ipfs2")
    valid_pallets = get_valid_pallets()
    if len(valid_pallets) >= 1:
        mh = valid_pallets[0].pallet_multihash
        logging.info("Testing pallet download from IPFS2: %s", mh)
        pallet_bytes = ipfs2.cat(mh)
        assert len(pallet_bytes) > 0


def step_70_inject_bad_files():
    """Types of issues to inject:
    * Bad nulldata (size)
    * Bad signature (corrupt)
    * good signature, Bad header multihash (no file)
    * bad header serialisation
    * Good header but no pallet (shouldn't happen but make sure things work if it does)
    * good header and corrupt pallet"""

    for step in ['bad_nd', 'bad_sig', 'no_header', 'bad_header', 'no_pallet', 'bad_pallet']:
        pallet_filename = run_pallet_producer(BOX_P_PALLET)
        if step == 'bad_pallet':
            logging.info("Corrupting pallet")
            with open(pallet_filename, 'r+b') as f:
                f.seek(200)  # this is past the length prefix but within the first block of votes
                f.write(b'\x00')
        ipfs_pallet_res = add_and_pin_to_ipfs_then_rm(pallet_filename)
        if step == 'no_pallet':
            logging.info("Breaking pallet multihash")
            ipfs_pallet_res['Hash'] = ipfs_pallet_res['Hash'][:-4] + 'a'*4
        if step == 'bad_sig':
            logging.info("Writing bad sig")
            pallet_header_filename = run_pallet_header_producer(ipfs_pallet_res['Hash'], verify_secret_key_hex[:-4] + "ffff")
        else:
            pallet_header_filename = run_pallet_header_producer(ipfs_pallet_res['Hash'], verify_secret_key_hex)
        if step == 'bad_header':
            logging.info("Corrupting header")
            with open(pallet_header_filename, 'wb') as f:
                f.write(b'\x00'*102)
        ipfs_pallet_header_res = add_and_pin_to_ipfs_then_rm(pallet_header_filename)
        if step == 'no_header':
            logging.info("Breaking header multihash")
            ipfs_pallet_header_res['Hash'] = ipfs_pallet_header_res['Hash'][:-4] + 'a'*4
        ipfs_pallet_header_bytes = bitcoin.base58.decode(ipfs_pallet_header_res['Hash'])
        if step == 'bad_nd':
            logging.info("Corrupting nulldata")
            ipfs_pallet_header_bytes = ipfs_pallet_header_bytes[:-4]
        nulldata = magic_bytes.encode() + ipfs_pallet_header_bytes
        bitcoin_publish_nulldata(nulldata, 0.001)



def do_test_round(n):
    step_00_bitcoind()
    step_10_scraper()
    step_20_pallet_headers()
    step_30_pallet_index()
    step_35_pallet_download()
    step_40_pallet_validation()
    step_50_check_state()
    step_60_check_ipfs()
    if n % 20 - 10 == 0:
        step_70_inject_bad_files()
        a = 1

# Run the main loop to produce Bitcoin blocks
ROUND_TIME_SEC = pallet_period
NUM_ROUNDS = max(NUM_PALLETS * 6, 20)
for round_n in range(NUM_ROUNDS):
    bitcoind.generate(120)
    r_start = time.time()
    fancy_log("Round %d / %d" % (round_n + 1, NUM_ROUNDS), {'time': time.time(), 'date': repr(datetime.datetime.now())})
    do_test_round(round_n)
    time.sleep(max(ROUND_TIME_SEC - time.time() + r_start, 0))

    if last_scrape_n >= NUM_PALLETS and last_header_n >= NUM_PALLETS and last_pallet_index_n >= NUM_PALLETS and \
            last_pallet_downloaded_n >= NUM_PALLETS and last_pallet_valid_n == NUM_PALLETS and \
            last_state.n_pallets == NUM_PALLETS:
        logging.info("Bailing out - early mark!")
        break


assert last_scrape_n >= NUM_PALLETS
assert last_header_n >= NUM_PALLETS
assert last_pallet_index_n >= NUM_PALLETS
assert last_pallet_downloaded_n >= NUM_PALLETS
assert last_pallet_valid_n == NUM_PALLETS
assert last_state.n_pallets == NUM_PALLETS

assert requests.get("http://vote-explorer:8683/getinfo").status_code == 200
explorer_getinfo = requests.get("http://vote-explorer:8683/getinfo").json()  # ensure this doesn't throw an exception

fancy_log("Vote Explorer GetInfo", explorer_getinfo)
assert explorer_getinfo['n_pallets'] == NUM_PALLETS
assert explorer_getinfo['n_votes'] == last_state.n_votes
assert explorer_getinfo['votes_for'] == last_state.votes_for
assert explorer_getinfo['votes_against'] == last_state.votes_against
