import os
import socket
import logging
import time
from collections import namedtuple

import psycopg2

from fancy_log import fancy_log
from environment import dbhost, dbname, dbuser, magic_bytes, MB_LEN, pallet_period, BOX_P_PALLET, NUM_PALLETS


"""
This file contains most of the SQL operations we need to perform.

As a matter of convention:
- Nulldata is stored as raw bytes
- Block hashes are stored hex encoded as varchar(64)s
- TXIDs are stored hex encoded as varchar(64)
"""


def returns_tuple(name, fields, single_row=False):
    """DECORATOR
    This will pass `ret_type` and `field_names` in as keyword args to a db get function.
    `ret_type` is a custom named tuple with fields matching the DB columns

    e.g.
    @returns_tuple("StateCached", ['id', 'last_processed', 'n_pallets', 'n_votes', 'votes_for', 'votes_against'])
    def get_state(ret_type=None, field_names=None):
        ...
    """
    ret_type = namedtuple(name, fields)
    def _decorator(f):
        def _inner(*args, **kwargs):
            ret = f(*args, ret_type=ret_type, field_names=fields, **kwargs)
            # take first row of results if single_row otherwise pass in infinite slice (take the lot)
            return list([ret_type(*row) for row in ret])[(0 if single_row else slice(None, None))]
        return _inner
    return _decorator


while True:
    try:
        dbhost_ip = socket.gethostbyname(dbhost)
        sqlconn = psycopg2.connect(database=dbname, host=dbhost_ip, user=dbuser)
    except (socket.gaierror, psycopg2.OperationalError) as e:
        logging.error("Unable to connect to Database, sleeping 5s: %s" % e)
        time.sleep(5)
    else:
        break


def run_query(sql_query, args=()):
    cur = sqlconn.cursor()
    try:
        resp = cur.execute(sql_query, args)
        logging.debug("Executed:\n%s" % (sql_query, ))
    except psycopg2.OperationalError as e:
        fancy_log("SQL Error", e)
        return list()
    except psycopg2.ProgrammingError as e:
        fancy_log("SQL Error", e)
        sqlconn.commit()
        raise e
    if sql_query.strip()[:6] not in ["INSERT", "UPDATE"]:
        try:
            _rows = list(cur.fetchall())
        except psycopg2.ProgrammingError as e:  # probably the case that there's nothing to fetch
            logging.warning("Warning, cur.fetchall() failed for %s", cur.query)
            sqlconn.commit()
            return list()
        logging.debug("Response has %d rows" % len(_rows))
        rows = [[a if type(a) is not memoryview else bytes(a) for a in row] for row in _rows]
    else:
        rows = list()
    sqlconn.commit()
    return list(rows)


def insert_nulldata(nulldata, txid, block_time, block_hash):
    prefix = nulldata[:MB_LEN]
    run_query("INSERT INTO null_data (prefix, tx_id, data, block_time, block_hash, processed) "
              " VALUES (%s, %s, %s, %s, %s, false) ON CONFLICT (tx_id, block_hash) DO NOTHING",
              (prefix, txid, nulldata, block_time, block_hash))


def get_earliest_unscanned_block():
    ret = run_query("SELECT hash, prevhash, height FROM block_data "
                    " WHERE scanned = false ORDER BY height ASC LIMIT 1")
    if len(ret) == 0:
        return None
    return {'hash': ret[0][0], 'height': ret[0][2], 'prevhash': ret[0][1]}


def get_latest_unknown_prevhash():
    ret = run_query("SELECT prevhash FROM block_data "
                    " WHERE have_prevblock=false"
                    " ORDER BY height DESC LIMIT 1")
    if len(ret) == 0:
        return None
    return ret[0][0]


def update_block(hash, scanned):
    return run_query("UPDATE block_data SET scanned = %s WHERE hash = %s", (scanned, hash))


def insert_unscanned_block(hash, prevhash, height, time):
    run_query("UPDATE block_data SET have_prevblock = true WHERE prevhash = %s", (hash,))
    parent = run_query("SELECT hash FROM block_data WHERE hash = %s", (prevhash,))
    have_prevblock = len(parent) > 0
    return run_query(" INSERT INTO block_data (hash, prevhash, height, time, scanned, have_prevblock)"
                     " VALUES (%s, %s, %s, %s, %s, %s) ON CONFLICT (hash) DO UPDATE "
                     " SET have_prevblock = %s",
                     (hash, prevhash, height, time, False, have_prevblock, have_prevblock))


def have_block(block_hash):
    ret = run_query("SELECT * FROM block_data WHERE hash = %s", (block_hash,))
    return len(ret) > 0


def get_unscanned_heights():
    ret = run_query("SELECT height FROM block_data WHERE scanned = false ORDER BY height ASC LIMIT 50")
    return list([hs[0] for hs in ret])


def get_unprocessed_nd():
    unprocessed = run_query("WITH updated AS (UPDATE null_data "
                            " SET processed = true "
                            " WHERE processed = false AND prefix = %s AND trouble = false "
                            " RETURNING id, data, block_time ) "
                            " SELECT * from updated "
                            , (magic_bytes,))
    if len(unprocessed) > 0:
        logging.info("%d unprocessed records" % len(unprocessed))
        logging.info(unprocessed[0])
    else:
        logging.info("No unprocessed nulldatas yet")
    return unprocessed


def reprocess_nd(nd_id):
    return run_query("UPDATE null_data SET processed = false WHERE nd_id = %s", (nd_id,))


def get_unprocessed_headers():
    unprocessed = run_query("SELECT header_multihash FROM header_index "
                            " WHERE header_multihash not in (SELECT header_multihash FROM header_processed) "
                            " ORDER BY last_processed ASC LIMIT 50")
    if len(unprocessed) > 0:
        logging.info("Got %d unprocessed headers" % len(unprocessed))
    return unprocessed


def add_to_header_index(header_mh, block_time):
    logging.info("Adding header to header_index %s" % header_mh)
    add_to_header_log(header_mh, block_time)
    return run_query("INSERT INTO header_index (header_multihash, last_processed) VALUES (%s, %s)", (header_mh, 0))


def add_to_header_processed(header_mh, valid, pallet_mh):
    args = (header_mh, valid, pallet_mh)
    logging.info("Adding header to header_processed (%s, %s, %s) and updating header_index.last_processed" % args)
    update_header_last_processed(header_mh)
    return run_query("INSERT INTO header_processed (header_multihash, valid, pallet_multihash) "
                     " VALUES (%s, %s, %s)", args)


def update_header_last_processed(mh):
    logging.info("Updating header last processed: %s", mh)
    run_query("UPDATE header_index SET last_processed = %s WHERE header_multihash = %s", (int(time.time()), mh))


def update_headers_last_processed(mhs):
    for mh in mhs:
        update_header_last_processed(mh)


def add_to_pallet_index(pallet_mh):
    logging.info("Adding pallet mh to pallet_index %s" % pallet_mh)
    return run_query("INSERT INTO pallet_index (pallet_multihash, last_processed, processed) VALUES (%s, %s, false)", (pallet_mh, 0))


def get_pallets_not_downloaded():
    """ Find all pallets that have not been downloaded"""
    not_downloaded = run_query(" WITH updated AS (UPDATE pallet_index "
                               "   SET processed = true "
                               "   WHERE processed = false AND trouble = false "
                               "   RETURNING pallet_multihash )"
                               " SELECT pallet_multihash FROM updated")
    logging.info("%d pallets that have not been downloaded", len(not_downloaded))
    return not_downloaded


def redownload_pallet(pallet_multihash):
    return run_query("UPDATE pallet_index SET processed = false WHERE pallet_multihash = %s", (pallet_multihash,))


def mark_pallet_downloaded(pallet_multihash):
    """ Mark pallets that have been downloaded """
    logging.info("Marking downloaded: %s", pallet_multihash)
    return run_query("INSERT INTO pallet_downloaded (pallet_multihash) VALUES (%s)", (pallet_multihash,))


def get_pallets_downloaded_needing_validation():
    """ Find all pallets that have been downloaded """
    pallets_downloaded = run_query(" WITH updated AS (UPDATE pallet_downloaded "
                                   "   SET processed = true "
                                   "   WHERE processed = false "
                                   "   RETURNING pallet_multihash )"
                                   " SELECT pallet_multihash FROM updated")
    logging.info("%d pallets that have not been validated", len(pallets_downloaded))
    return pallets_downloaded


def revalidate_pallet(pallet_mh):
    return run_query("UPDATE pallet_download SET processed = false WHERE pallet_multihash = %s", (pallet_mh,))


def add_to_pallet_validated(pallet_multihash, valid, n_votes, votes_for, votes_against):
    args = (pallet_multihash, valid, n_votes, votes_for, votes_against)
    logging.info("Adding validated pallet data to pallet_validated: %s", pallet_multihash)
    log_pallet_validated(pallet_multihash, n_votes)
    return run_query(" INSERT INTO pallet_validated (pallet_multihash, valid, n_votes, votes_for, votes_against)"
                     " VALUES (%s, %s, %s, %s, %s)", args)


def log_pallet_validated(pallet_mh, n_votes):
    run_query("INSERT INTO pallet_validated_log (pallet_multihash, n_votes, timestamp) VALUES (%s, %s, %s)",
              (pallet_mh, n_votes, int(time.time())))


@returns_tuple("Pallet", ["id", "pallet_multihash", 'valid', 'n_votes', 'votes_for', 'votes_against'])
def get_valid_pallets(ret_type, field_names):
    return run_query("SELECT {fields} FROM pallet_validated WHERE valid = true".format(fields=', '.join(field_names)))


@returns_tuple("Pallet", ["id", "pallet_multihash", 'valid', 'n_votes', 'votes_for', 'votes_against'])
def get_invalid_pallets(ret_type, field_names):
    return run_query("SELECT {fields} FROM pallet_validated WHERE valid = false".format(fields=', '.join(field_names)))


def mark_pallet_trouble(pallet_mh):
    return run_query("UPDATE pallet_index SET trouble = true WHERE pallet_multihash = %s", (pallet_mh,))


@returns_tuple("State", ['id', 'last_processed', 'n_pallets', 'n_votes', 'votes_for', 'votes_against'], single_row=True)
def get_state(ret_type=None, field_names=None):
    ret = []
    while len(ret) == 0:
        ret = run_query("SELECT {fields} FROM state_cache WHERE id=1".format(fields=', '.join(field_names)))
        if len(ret) == 0:
            update_state()
    return ret


def get_n_valid_pallets():
    return run_query("SELECT COUNT(id) FROM pallet_validated WHERE valid = true")[0]


def update_state():
    return run_query("INSERT INTO state_cache (id) VALUES (1) ON CONFLICT (id) DO UPDATE SET "
                     "  n_pallets = (SELECT COUNT(pallet_multihash) FROM pallet_validated WHERE valid = true) "
                     ", n_votes = (SELECT COALESCE(SUM(n_votes), 0) FROM pallet_validated WHERE valid = true) "
                     ", votes_for = (SELECT COALESCE(SUM(votes_for), 0) FROM pallet_validated WHERE valid = true) "
                     ", votes_against = (SELECT COALESCE(SUM(votes_against), 0) FROM pallet_validated WHERE valid = true)"
                     ", last_processed = %s", [int(time.time())])


def update_state_log():
    state = get_state()
    logging.info("Updating state log")
    return run_query("INSERT INTO state_log (timestamp, n_pallets, n_votes, votes_for, votes_against) "
                     " VALUES (%s, %s, %s, %s, %s)", (int(time.time()), state.n_pallets, state.n_votes,
                                                      state.votes_for, state.votes_against))


def update_velocity_cache():
    logging.info("Velocity: starting")
    state_vps = get_vps_from_state()
    logging.info("Velocity: Got state_vps %s", state_vps)
    pallet_vps = get_vps_from_pallet()
    logging.info("Velocity: Got pallet_vps %s", pallet_vps)
    header_vps = get_vps_from_headers()
    logging.info("Velocity: Got header_vps %s", header_vps)
    logging.info("Velocity: Updating velocity cache")
    return run_query("INSERT INTO velocity_cache (state_vps, pallet_vps, header_vps, last_processed) "
                     " VALUES (%s, %s, %s, %s) ", [state_vps, pallet_vps, header_vps, int(time.time())])


def get_vps_from_state():
    now = int(time.time())
    state_logs = run_query("SELECT * FROM state_log WHERE timestamp > %d ORDER BY id DESC" % (now - 60 * 60))
    if len(state_logs) < 2:
        return 0
    history = []
    for log in state_logs:
        id, timestamp, n_pallets, n_votes, votes_for, votes_against = log
        history.append((timestamp, n_pallets, n_votes))
    min_hs, max_hs = min(history), max(history)
    return (max_hs[2] - min_hs[2]) / (now - min_hs[0])  # return velocity of votes


def get_vps_from_pallet():
    now = int(time.time())
    pallet_logs = run_query("SELECT * FROM pallet_validated_log WHERE timestamp > %d ORDER BY id DESC LIMIT 5" % (now - 60 * 60))
    if len(pallet_logs) < 2:
        return 0
    history = []
    for log in pallet_logs:
        id, pallet_mh, n_votes, timestamp = log
        history.append((timestamp, n_votes))
    min_hs, max_hs = min(history), max(history)
    logging.info("Min max hist: %s %s", min_hs, max_hs)
    return sum(map(lambda tv: tv[1], history)) / (now - min_hs[0])  # return velocity of votes


def add_to_header_log(header_mh, block_time):
    return run_query("INSERT INTO header_log (header_multihash, block_time) VALUES (%s, %s)", (header_mh, block_time))


def get_vps_from_headers():
    bts = run_query("SELECT DISTINCT block_time FROM header_log ORDER BY block_time DESC")
    if len(bts) < 2:
        return 0
    bt1, bt2 = bts[0][0], bts[1][0]
    hdr_logs = run_query("SELECT COUNT(*) FROM header_log WHERE block_time = %s" % bt1)
    return len(hdr_logs) * BOX_P_PALLET * 64 / (bt1 - bt2)


@returns_tuple('VelocityCache', ['id', 'last_processed', 'state_vps', 'pallet_vps', 'header_vps'])
def get_velocity_latest():
    return run_query("SELECT * FROM velocity_cache ORDER BY id DESC LIMIT 1")

