import os
from binascii import hexlify
import logging
from pprint import pformat


def gen_header_filename():
    return hexlify(os.urandom(10)).decode() + ".pallet-header"


def fancy_log(title, thing):
    logging.info("")
    logging.info("########" * 2 + "#" * len(title))
    logging.info("####### %s #######" % title)
    logging.info("########" * 2 + "#" * len(title))
    logging.info("")
    logging.info(pformat(thing, indent=2))
    logging.info("")