from ed25519 import *

from svst.structs import *


def create_sigpair(sig_bytes, pk_bytes):
    return {
        'signature': sig_bytes,
        'pub_key': pk_bytes
    }


def generate_sigpair(msg):
    sk, vk = create_keypair()
    sig = sk.sign(msg)
    return {
        'signature': sig,
        'pub_key': vk.to_bytes()
    }