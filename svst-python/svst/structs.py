# A pallet is a datastructure containing boxes
# A box is a datastructure containing N votes and N (signatures, public key) pairs
import logging
from collections import namedtuple

logging.basicConfig(level=logging.INFO)

import io
import struct
from functools import partial

import construct
from construct import *


class PalletUnpackError(Exception):
    pass


Signature = Byte[64]
PubKey    = Byte[32]
MultiHash = Byte[34]


Vote = Struct(
    "direction" / Int8sb,
    "nonce"     / Byte[4]
)


SigPair = Struct(
    "signature" / Signature,
    "pub_key"   / PubKey
)


BallotBox = Struct(
    "count"    / Rebuild(Int32ub, len_(this.votes)),
    "votes"    / Vote[:],
    "sigpairs" / SigPair[:],
    Check(lambda ctx: len(ctx.votes) == len(ctx.sigpairs))
)


Pallet = Struct(
    "count" / Rebuild(Int32ub, len_(this.boxes)),
    "boxes" / BallotBox[:],  # using Int32ub means max Pallet length is 2^32 bytes => 4.29 GB
)


PalletHeader = Struct(
    "version"     / Const(b"\x00"*4),
    "pallet_hash" / MultiHash,
    "auth_sig"    / Signature,  # From flux central command
)


WORD32_SIZE = 4
VOTE_SIZE   = 5
SIG_SIZE    = 64  # ed25519 sigs are 64 bytes
PUBKEY_SIZE = 32  # ed25519 pubkeys are 32 bytes

BOX_SIZE    = WORD32_SIZE + 64 * (VOTE_SIZE + SIG_SIZE + PUBKEY_SIZE)  # 4 byte count at start and 64 votes / box
VOTES_P_BOX = 64


def unpack_uint32(bs):
    return struct.unpack('>L', bs)[0]


def unpack_and_count_pallet(buffer):
    try:
        n_boxes_raw = buffer.read(4)
        n_boxes_claimed = unpack_uint32(n_boxes_raw)
        n_votes_this_pallet = votes_for = votes_against = votes_anomalies = n_boxes = 0
        for raw_box in iter(partial(buffer.read, BOX_SIZE), b''):
            n_boxes += 1
            votes_this_box = unpack_uint32(raw_box[:4])
            assert votes_this_box == VOTES_P_BOX
            raw_votes_buffer = io.BytesIO(raw_box[4:VOTE_SIZE * votes_this_box + 4])
            for raw_vote in iter(partial(raw_votes_buffer.read, VOTE_SIZE), b''):
                vote = raw_vote[0]
                vote_check = (raw_vote[1] ^ raw_vote[2] ^ raw_vote[3] ^ raw_vote[4]) % 2
                assert vote == vote_check
                n_votes_this_pallet += 1
                if vote == 0:
                    votes_against += 1
                elif vote == 1:
                    votes_for += 1
                else:
                    votes_anomalies += 1
            assert len(raw_box[4 + VOTE_SIZE * votes_this_box:]) == (SIG_SIZE + PUBKEY_SIZE) * votes_this_box
        assert votes_anomalies == 0
        assert n_boxes == n_boxes_claimed
        PalletStats = namedtuple("PalletStats", 'n_votes, votes_for, votes_against, votes_anomalies')
        return PalletStats(*(n_votes_this_pallet, votes_for, votes_against, votes_anomalies))
    except AssertionError as e:
        raise PalletUnpackError(str(e))
