# SVST Python

## Setup

`pip3 install -r requirements.txt`

You will also need `svst-haskell` installed and the exes in your path

## Debug Utilities

Debug utils are stored in ./utils

* `clean-pallets.sh` will delete all pallets in the current directory
* `run-dev.sh` will run the producer wrapper with some default params
* `run-dev-continuous.sh` will exec `run-dev.sh` when files are saved

## Utils

NOTE: These might be deprecated or out of date. They're not canonical - keep that in mind.

To make a pallet:

`python3 -m svst.make_pallet`

Note: create a pallet of 10,000 boxes is VERY slow (hence writing the haskell version).

However, construct is lovely so we use that to define the data structs
