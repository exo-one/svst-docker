#!/usr/bin/env bash

#
# TESTNET
# Run from svst-docker/
#

./testnet/x_testnet_pause.sh "$1" "$2"

echo "Regenerating docker-compose.testnet.yml"
./testnet/1_create_compose.sh

echo "Rsync"
./testnet/x_testnet_rsync.sh "$1" "$2"

function startauditor {
    ssh "$1@$2" "cd src/svst/svst-docker && echo '$2 starting AUDITOR' && tmux new-session -d './testnet/r2_auditor.sh | tee log-full'"
}

function startproducer {
    ssh "$1@$2" "cd src/svst/svst-docker && echo '$2 starting PRODUCER' && tmux new-session -d './testnet/r2_producer.sh | tee log-full'"
}

if [ -n "$DEPLOY_PRODUCER" ]; then
  startproducer "$1" "$2"
else
  echo "Deploying Auditor; use DEPLOY_PRODUCER to avoid"
  startauditor "$1" "$2"
fi

echo "BUILD STARTED (should take 10m or so)"
echo "In about 10 minutes you should have access to the vote explorer at http://$2:8683"
echo "To watch run the build and debug logs command: "
echo "./testnet/x_testnet_logs.sh \"$1\" \"$2\""
