#!/usr/bin/env python3

import yaml

docker_filename = 'docker-compose.testnet.yml'
docker = yaml.load(open(docker_filename, 'r'))

try:
    del docker['services']['bitcoind']['build']
except KeyError:
    pass
docker['services']['bitcoind']['image'] = "exo1/dev-test-bitcoind"
docker['services']['bitcoind']['entrypoint'] = \
    "sh -c 'btc_init && bitcoind -testnet -printtoconsole=0 -datadir=/bitcoin/.bitcoin'"

docker['services']['producer']['entrypoint'] = "sh -c './producer.sh'"

for service in docker['services']:
    if 'env_file' in docker['services'][service]:
        docker['services'][service]['env_file'].append("env_testnet.env")

with open(docker_filename, 'w') as f:
    f.write("# DO NOT EDIT DIRECTLY - USE ./testnet/1_create_compose.sh!\n\n")
    yaml.dump(docker, f)
print("Edited %s successfully" % docker_filename)
