#!/usr/bin/env bash

#
# TESTNET
# Run from svst-docker/
#

ssh "$1@$2" "cd src/svst/svst-docker && tail -f log-full"
