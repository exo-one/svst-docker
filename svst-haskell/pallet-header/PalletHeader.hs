{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Monad
import           Crypto.Ed25519.Pure
import           Data.ByteString (ByteString)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
import           Data.Hex
import           Data.Maybe
import           Options.Applicative

-- friends
import           SVST.Pallet
import           SVST.Util

data Opts =
  Opts {
    optOutFile    :: FilePath
  , optMultihash  :: ByteString
  , optSigningKey :: ByteString
  } deriving (Read, Show, Eq)

optsParser :: Parser Opts
optsParser =
      Opts
  <$> strOption (short 'o' <> metavar "<file>" <>
                 help "output pallet header file")
  <*> (BC.pack <$> argument str (metavar "<multihash>"))
  <*> (BC.pack <$> strOption (short 'k' <> long "key" <>
                     metavar "<private key hex>" <>
                     help "ED25519 private signing key in hex"))

optsInfo :: ParserInfo Opts
optsInfo =
  info (helper <*> optsParser)
    (fullDesc <>
     progDesc "Creates a pallet header from IPFS multihash for pallet")

main :: IO ()
main = do
  opts <- execParser optsInfo
  let mbKey = do -- Maybe monad
        bytes <- unhex $ optSigningKey opts
        importPrivate bytes
  when (isNothing mbKey) $
    exitWithError "Signing key either not hex or not 64 hex chars long"
  let mbHeader = mkPalletHeader (fromJust mbKey) (optMultihash opts)
  when (isNothing mbHeader) $
    exitWithError "Could not decode base58 encoded multihash"
  let Just header = mbHeader
      outFile = optOutFile opts
  B.writeFile outFile header
  putStrLn $ "Wrote pallet header for multihash " ++ BC.unpack (optMultihash opts)
              ++ " to '" ++ outFile ++"'"
  return ()