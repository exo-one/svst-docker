#!/bin/bash

echo "******************************************************************"
echo "*** This will destroy 'svst_nulldata' database and recreate it ***"
echo "******************************************************************"
echo -n "Are you okay with this? [y/N] "

read RES
case $RES in
  y|Y)
   ;;
  *)
   exit 0
   ;;
esac

USER="svst"
PASSWORD="G9iRWVU6yxkZ"
DB="svst_nulldata"

sudo -u postgres psql <<END

DROP DATABASE IF EXISTS $DB;
CREATE DATABASE $DB;
CREATE USER $USER;
ALTER USER $USER PASSWORD '$PASSWORD';
GRANT ALL PRIVILEGES ON DATABASE $DB TO $USER;

\c $DB;

CREATE TABLE null_data (id SERIAL PRIMARY KEY,
                        tx_id bytea NOT NULL,
                        data bytea NOT NULL,
                        block_height integer NOT NULL);
ALTER TABLE null_data OWNER TO $USER;

CREATE UNIQUE INDEX unique_tx_id_block_time ON null_data (tx_id, block_time);
CREATE INDEX tx_id_seq ON null_data (tx_id);
CREATE INDEX data_seq ON null_data (data);

CREATE TABLE pallet(
               id SERIAL PRIMARY KEY,
               null_data_id INTEGER REFERENCES null_data(id) NOT NULL,
               header_ipfs_multihash VARCHAR(47) NOT NULL,
               ipfs_multihash VARCHAR(47),
               verified BOOLEAN);

ALTER TABLE pallet OWNER to $USER;


END

if [ -f $HOME/.pgpass ]; then
  cat $HOME/.pgpass | grep -v "$DB:$USER" > $HOME/.pgpass
else
  touch $HOME/.pgpass
fi

echo "localhost:5432:$DB:$USER:$PASSWORD" > $HOME/.pgpass
chmod 600 $HOME/.pgpass

echo; echo; echo
echo "To access the database use:"
echo "  psql -U $USER $DB"