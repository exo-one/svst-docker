#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

/* #define ED25519_DLL */
#include "ed25519.h"
#include "test-ticks.h"

#include <pthread.h>
#include <errno.h>

#define THREADS 4

#define TEST_BATCH_COUNT 64
#define TEST_BATCH_ROUNDS 10000

int count = 0; // global count
pthread_mutex_t mutex; // global mutex

ed25519_secret_key sks[TEST_BATCH_COUNT];
ed25519_public_key pks[TEST_BATCH_COUNT];
ed25519_signature sigs[TEST_BATCH_COUNT];

unsigned char messages[TEST_BATCH_COUNT][128];

const unsigned char *message_pointers[TEST_BATCH_COUNT];
const unsigned char *pk_pointers[TEST_BATCH_COUNT];
const unsigned char *sig_pointers[TEST_BATCH_COUNT];
size_t message_lengths[TEST_BATCH_COUNT];
int64_t total_ticks = 0;

static void incCount(int thread_id) {
 if ( !pthread_mutex_lock(&mutex) ) {
//   printf("Thread id %d verifying batch %d\n", thread_id, count);
   count++;
   if (pthread_mutex_unlock(&mutex) ) {
     printf("Mutex is invalid or current thread does not have lock!\n");
     exit(1);
   }
 }
}

/* Precondition: Mutex must be initialised */
void *func(void *arg) {
  int i;
  int thread_id = (int) arg;
  int valid[TEST_BATCH_COUNT]; // not used
  uint64_t t;

  for (;;) {
    if (count < TEST_BATCH_ROUNDS) {
      incCount(thread_id);
      t = get_ticks();
      ed25519_sign_open_batch(message_pointers, message_lengths,
        pk_pointers, sig_pointers, TEST_BATCH_COUNT, valid);
      total_ticks += (get_ticks() - t);

      // check validity
      for (i = 0; i < TEST_BATCH_COUNT; i++) {
        if (valid[i] != 1) {
          printf("Invalid sig! %d (%d)\n", i, count);
          exit(1);
        }
      }

    } else {
      break; // quit infinite loop
    }
  }
  return NULL;
}

void init_mutex() {
  int err;
  if ( (err = pthread_mutex_init(&mutex, NULL)) ) {
    switch (err) {
      case EAGAIN:
        printf("System lacks resources\n");
        break;;
      case EINVAL:
        printf("attribute invalid\n");
      case ENOMEM:
        printf("Cannot allocate enough memory\n");
    }
    exit(1);
  }
}

void init_sigs() {
  int i;


  /* generate keys */
  for (i = 0; i < TEST_BATCH_COUNT; i++) {
    ed25519_randombytes_unsafe(sks[i], sizeof(sks[i]));
    ed25519_publickey(sks[i], pks[i]);
    pk_pointers[i] = pks[i];
  }

  /* generate messages */
  ed25519_randombytes_unsafe(messages, sizeof(messages));
  for (i = 0; i < TEST_BATCH_COUNT; i++) {
    message_pointers[i] = messages[i];
    printf("%zu\n", (i & 127) + 1 );
    message_lengths[i] = (i & 127) + 1;
  }

  /* sign messages */
  for (i = 0; i < TEST_BATCH_COUNT; i++) {
    ed25519_sign(message_pointers[i], message_lengths[i], sks[i], pks[i], sigs[i]);
    sig_pointers[i] = sigs[i];
  }
}


int main(void) {
  pthread_t threads[THREADS];
  int i;
  struct timeval start, finish;

  init_sigs();
  init_mutex();

  int num_sigs = TEST_BATCH_COUNT * TEST_BATCH_ROUNDS;
  printf("Verifying %d sigs\n", num_sigs);

  gettimeofday(&start, NULL);

  for (i=0; i<THREADS; i++) {
    pthread_create(&threads[i], NULL, func, (void *) i);
  }

  for (i=0; i<THREADS; i++) {
    pthread_join(threads[i], NULL);
  }

  gettimeofday(&finish, NULL);
  double duration = (finish.tv_sec - start.tv_sec) +
                    (finish.tv_usec - start.tv_usec)/1e6;

  double sigs_per_sec = num_sigs/duration;

  printf("Duration: %g s\n", duration);
  printf("Sigs/s: %.2f\n",   sigs_per_sec);
  printf("Ticks/sig: %.2f\n", ((double) total_ticks)/((double) num_sigs));
  printf("Tres comas in %.2f hours\n", (1000000000/sigs_per_sec) / 3600);
  return 0;
}
