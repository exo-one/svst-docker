# Secure Vote Pallet Signature Verifier

    $ cd src
    $ make perf
    $ ./perf

<img src="three-comma-club.jpg">

### Testing

`test` is compiled with `-DED25519_TEST` which is explained [here](https://github.com/floodyberry/ed25519-donna#random-options).

```
Use -DED25519_TEST when compiling ed25519.c to use a deterministically seeded, non-thread safe
CSPRNG variant of Bob Jenkins ISAAC
```

Presumably using a deterministically seeded PRNG renders it insecure. For that reason it's important
not to compile `verify-pallet` with `-DED25519_TEST`.

