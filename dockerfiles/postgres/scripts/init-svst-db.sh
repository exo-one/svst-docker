#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "postgres" <<-EOSQL

-- All nulldata stored here
CREATE TABLE null_data
    ( id SERIAL PRIMARY KEY
    , prefix bytea NOT NULL
    , tx_id varchar(64) NOT NULL
    , data bytea NOT NULL
    , block_time integer NOT NULL
    , block_hash varchar(64) NOT NULL
    , trouble BOOLEAN DEFAULT false NOT NULL
    , processed BOOLEAN DEFAULT false NOT NULL
    );

CREATE UNIQUE INDEX unique_tx_id_block_hash ON null_data (tx_id, block_hash);
CREATE INDEX tx_id_seq ON null_data (tx_id);
CREATE INDEX data_seq ON null_data (data);
CREATE INDEX prefix_seq ON null_data (prefix);

-- LEGACY TABLE, added in to avoid doing more work
CREATE TABLE pallet
    ( id SERIAL PRIMARY KEY
    , null_data_id INTEGER REFERENCES null_data(id) NOT NULL
    , header_ipfs_multihash VARCHAR(50) NOT NULL
    , ipfs_multihash VARCHAR(50)
    , pallet_dl BOOLEAN DEFAULT false
    , header_dl BOOLEAN DEFAULT false
    , verified BOOLEAN DEFAULT false
    );

-- Track block data
CREATE TABLE block_data
    ( id SERIAL PRIMARY KEY
    , hash varchar(64) NOT NULL
    , prevhash varchar(64) NOT NULL
    , height integer NOT NULL
    , time integer NOT NULL
    , scanned BOOLEAN DEFAULT false NOT NULL
    , have_prevblock BOOLEAN DEFAULT false NOT NULL
    );
CREATE UNIQUE INDEX unique_block_hash ON block_data (hash);
CREATE INDEX height_seq ON block_data (height);
CREATE INDEX prevhash_seq ON block_data (prevhash);
CREATE INDEX time_seq ON block_data (time);
CREATE INDEX scanned_seq ON block_data (scanned);
CREATE INDEX have_prevblock_seq ON block_data (have_prevblock);

-- All headers we know about are stored here
CREATE TABLE header_index
    ( id SERIAL PRIMARY KEY
    , header_multihash varchar(50) NOT NULL
    , last_processed integer NOT NULL
    );

-- Headers we process are here
CREATE TABLE header_processed
    ( _id SERIAL PRIMARY KEY
    , header_multihash varchar(50) NOT NULL
    , valid BOOLEAN DEFAULT false
    , pallet_multihash varchar(50) NOT NULL
    );

-- All pallets we know about go here
CREATE TABLE pallet_index
    ( id SERIAL PRIMARY KEY
    , pallet_multihash varchar(50) NOT NULL
    , last_processed integer NOT NULL
    , trouble BOOLEAN DEFAULT false NOT NULL
    , processed BOOLEAN DEFAULT false NOT NULL
    );

-- Data about pallets we've downloaded
CREATE TABLE pallet_downloaded
    ( id SERIAL PRIMARY KEY
    , pallet_multihash varchar(50) NOT NULL
    , processed BOOLEAN DEFAULT false NOT NULL
    );

-- Data about pallets we've validated
CREATE TABLE pallet_validated
    ( id SERIAL PRIMARY KEY
    , pallet_multihash varchar(50) NOT NULL
    , valid BOOLEAN DEFAULT false
    , n_votes integer NOT NULL
    , votes_for integer NOT NULL
    , votes_against integer NOT NULL
    );

-- Cache of state (one row only)
CREATE TABLE state_cache
    ( id SERIAL PRIMARY KEY
    , last_processed integer NOT NULL DEFAULT 0
    , n_pallets integer NOT NULL DEFAULT 0
    , n_votes integer NOT NULL DEFAULT 0
    , votes_for integer NOT NULL DEFAULT 0
    , votes_against integer NOT NULL DEFAULT 0
    );

-- Cache of velocity (one row only)
CREATE TABLE velocity_cache
    ( id SERIAL PRIMARY KEY
    , last_processed integer NOT NULL DEFAULT 0
    , state_vps real NOT NULL DEFAULT 0
    , pallet_vps real NOT NULL DEFAULT 0
    , header_vps real NOT NULL DEFAULT 0
    );

-- Log of headers against the blockchain
CREATE TABLE header_log
    ( id SERIAL PRIMARY KEY
    , header_multihash varchar(50) NOT NULL
    , block_time integer NOT NULL
    );

-- Log of actual pallet verifications on the local machine
CREATE TABLE pallet_validated_log
    ( id SERIAL PRIMARY KEY
    , pallet_multihash varchar(50) NOT NULL
    , n_votes integer NOT NULL
    , timestamp integer NOT NULL
    );

-- Log of actual pallet verifications on the local machine
CREATE TABLE state_log
    ( id SERIAL PRIMARY KEY
    , timestamp integer NOT NULL DEFAULT 0
    , n_pallets integer NOT NULL DEFAULT 0
    , n_votes integer NOT NULL DEFAULT 0
    , votes_for integer NOT NULL DEFAULT 0
    , votes_against integer NOT NULL DEFAULT 0
    );

EOSQL
